#!/usr/bin/env sh

mkdir -v $HOME/qaac_build
cd $HOME/qaac_build

if [ ! -d "~/.wine/drive_c/qaac/" ]; then
	mkdir -pv ~/.wine/drive_c/qaac/
fi

wget https://github.com/nu774/qaac/releases/download/v2.68/qaac_2.68.zip
unzip -j qaac_2.68.zip 'qaac_2.68/x86/*' -d ~/.wine/drive_c/qaac/

wget https://secure-appldnld.apple.com/itunes12/041-02279-20180912-24D8EE3A-AC7A-11E8-BE19-C36F1B1141A5/iTunesSetup.exe
7z e -y iTunesSetup.exe AppleApplicationSupport.msi

7z e -y AppleApplicationSupport.msi \
	 -i'!*AppleApplicationSupport_ASL.dll' \
	 -i'!*AppleApplicationSupport_CoreAudioToolbox.dll' \
	 -i'!*AppleApplicationSupport_CoreFoundation.dll' \
	 -i'!*AppleApplicationSupport_icudt*.dll' \
	 -i'!*AppleApplicationSupport_libdispatch.dll' \
	 -i'!*AppleApplicationSupport_libicu*.dll' \
	 -i'!*AppleApplicationSupport_objc.dll' \
	 -i'!F_CENTRAL_msvc?100*'

for j in *.dll; do
	mv -v $j $(echo $j | sed 's/AppleApplicationSupport_//g')
done

for j in F_CENTRAL_msvcr100*; do
	mv -v "$j" msvcr100.dll
done

for j in F_CENTRAL_msvcp100*; do
	mv -v "$j" msvcp100.dll
done

mv -v *.dll ~/.wine/drive_c/qaac/

wget http://www.andrews-corner.org/downloads/x32DLLs_20161112.zip
unzip -j x32DLLs_20161112.zip 'x32DLLs_20161112/*' -d ~/.wine/drive_c/qaac/

echo "alias qaac='wine ~/.wine/drive_c/qaac/qaac.exe'" >> ~/.bashrc
echo "alias refalac='wine ~/.wine/drive_c/qaac/refalac.exe'" >> ~/.bashrc
echo "export WINEDEBUG=-all" >> ~/.bashrc

cd ..
rm -rfv qaac_build
